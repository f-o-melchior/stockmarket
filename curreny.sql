-- PostgreSQL

-- Needs a database called 'wup_currency' on port 5432!

DROP TABLE IF EXISTS market;

CREATE TABLE market (
	currency_id SERIAL PRIMARY KEY,
	currency_name TEXT NOT NULL,
	sale NUMERIC  NOT NULL,
	buy NUMERIC NOT NULL
);

INSERT INTO market (currency_name, sale, buy)
VALUES ('USD', 272.170, 282.710),
('JPY', 2.460, 2.607),
('GBP', 354.920, 371.460),
('SEK', 29.690, 31.480),
('CAD', 202.860, 215.040);       