# StockMarket

This project has been created by Bendegúz Gábor-Svanda.

### How does it work?

Simple. You just need an IDE what can handle Spring framework...
The application provides foreign currency exchange. I tried to simulate changes of the currencies, as in real life, by using a delayed service.

```
This application is a REST application and it uses PostgresSQL as an SQL based database and MyBatis to handle all the database operations. 
```

#### Endpoints

**/** -> The "homepage of the application"

**/market/all** -> Shows all the currencies stored in the database

**/market/find/{id}** -> Shows currency with related id

**/market/exchange/{from}/{to}/{amount}** -> Exchange function. Calculates the amount of {to} from {from}.

#### To try the application just clone 'develop' or 'master' branch. :)